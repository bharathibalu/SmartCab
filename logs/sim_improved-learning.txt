/-----------------------------------------
| State-action rewards from Q-Learning
\-----------------------------------------

('forward', 'red', None, None, 'forward')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : -11.86

('left', 'red', 'left', 'left', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.34
 -- left : 0.00

('right', 'red', None, None, 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : -0.02
 -- left : 0.00

('left', 'green', None, 'right', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.80

('forward', 'red', 'left', 'left', None)
 -- forward : -2.92
 -- right : 0.00
 -- None : 0.57
 -- left : 0.00

('forward', 'green', 'left', 'left', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : -1.64
 -- left : 0.38

('left', 'red', 'left', None, 'forward')
 -- forward : 0.00
 -- right : 0.05
 -- None : 0.00
 -- left : 0.00

('left', 'red', 'left', 'right', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : -2.71

('forward', 'green', 'left', None, None)
 -- forward : 0.00
 -- right : 0.00
 -- None : -1.35
 -- left : 0.00

('forward', 'green', None, 'forward', None)
 -- forward : 0.33
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('forward', 'green', None, None, 'right')
 -- forward : 0.20
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('left', 'red', 'left', 'forward', 'forward')
 -- forward : 0.00
 -- right : -6.22
 -- None : 0.00
 -- left : 0.00

('left', 'green', None, None, None)
 -- forward : 0.37
 -- right : 0.15
 -- None : -1.61
 -- left : 0.00

('right', 'red', None, 'forward', None)
 -- forward : 0.00
 -- right : -6.18
 -- None : 0.00
 -- left : 0.00

('right', 'red', None, None, None)
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.32
 -- left : -2.73

('left', 'green', None, 'forward', None)
 -- forward : 0.00
 -- right : 0.10
 -- None : 0.00
 -- left : 0.00

('left', 'red', 'left', None, None)
 -- forward : 0.00
 -- right : 0.30
 -- None : 0.74
 -- left : -7.42

('right', 'green', None, None, 'right')
 -- forward : 0.00
 -- right : 0.49
 -- None : 0.00
 -- left : 0.26

('forward', 'red', None, None, None)
 -- forward : 0.00
 -- right : 0.03
 -- None : 0.45
 -- left : -5.12

('right', 'green', None, 'forward', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.26

('right', 'green', None, 'left', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : -1.27
 -- left : 0.00

('right', 'red', None, 'forward', 'left')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.23
 -- left : 0.00

('right', 'green', None, None, None)
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.36

('forward', 'green', 'forward', None, None)
 -- forward : 0.00
 -- right : 0.00
 -- None : -1.33
 -- left : 0.00

('left', 'red', 'forward', 'left', 'right')
 -- forward : -3.08
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('right', 'green', 'right', 'left', None)
 -- forward : 0.00
 -- right : 0.47
 -- None : 0.00
 -- left : 0.00

('forward', 'red', 'left', 'left', 'forward')
 -- forward : -11.98
 -- right : 0.00
 -- None : 0.00
 -- left : 0.00

('right', 'red', None, 'left', None)
 -- forward : 0.00
 -- right : 0.64
 -- None : 0.00
 -- left : 0.00

('forward', 'red', 'left', None, None)
 -- forward : -4.97
 -- right : 0.00
 -- None : 0.00
 -- left : -6.29

('left', 'red', 'left', None, 'right')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.53
 -- left : 0.00

('right', 'green', 'forward', None, 'forward')
 -- forward : 0.00
 -- right : 0.00
 -- None : -1.36
 -- left : 0.00

('forward', 'green', None, 'left', None)
 -- forward : 0.00
 -- right : -0.11
 -- None : 0.00
 -- left : 0.41

('forward', 'red', None, None, 'left')
 -- forward : 0.00
 -- right : 0.33
 -- None : 0.00
 -- left : 0.00

('forward', 'green', 'right', 'left', None)
 -- forward : 0.00
 -- right : 0.35
 -- None : 0.00
 -- left : 0.00

('right', 'green', None, 'forward', None)
 -- forward : 0.00
 -- right : 0.00
 -- None : -1.64
 -- left : 0.00

('right', 'green', None, None, 'forward')
 -- forward : 0.00
 -- right : 0.00
 -- None : 0.00
 -- left : 0.44

('forward', 'red', 'forward', None, None)
 -- forward : -2.97
 -- right : 0.00
 -- None : 0.28
 -- left : -3.11

('left', 'red', 'right', 'left', 'left')
 -- forward : 0.00
 -- right : 0.44
 -- None : 0.00
 -- left : 0.00

