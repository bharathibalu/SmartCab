**Traffic_Light    Waypoint    Oncoming_vehicle_waypoint            Left_vehicle_waypoint       Right_waypoint  Policy/Action**
  Red              Forward                                                                                      None
  Red              Left                                                                                         None
  Red              Right                                            Forward                                     None
  Red              Right                                            Anything other than Forward                 Right
  Green            Forward                                                                                      Forward
  Green            Right                                                                                        Right
  Green            Left        Anything other than forward or right                                             Left
  Green            Left        Right or Forward                                                                 Forward or Right